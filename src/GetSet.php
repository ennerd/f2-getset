<?php
namespace F2;

use ReflectionClass, ReflectionMethod, Error;
use function get_class, method_exists;

/**
 * Implement this trait, and create getters and setters like so:
 *
 * public function get_<propertyName>(): <type>;
 * public function set_<propertyName>(<type> $value): void;
 *
 * Method visibility and type hints are respected.
 */
trait GetSet {
    /**
     * Magic method for getters.
     *
     * Implementation details list:
     *
     * Visibility:
     * - public (OK)
     * - protected
     * - private
     *
     * Error styles:
     * - getter not found (OK)
     * - getter not found and property exists (OK)
     * - access to protected from foreign scope
     * - access to private from foreign scope
     * - access to private from child scope
     */
    public function __get(string $name) {
        $methodName = 'get_'.$name;

        // Do we have a getter?
        if (!method_exists($this, $methodName)) {
            if (property_exists($this, $name)) {
                // Error message is different if the property exists
                throw $this->bumpError(new Error("Cannot access private property ".static::class."::\$$name"));
            }
            trigger_error("Undefined property: ".static::class."::\$$name");
            return;
        }

        $ref = new ReflectionMethod($this, $methodName);

        if ($ref->isPublic()) {
            // Property is available everywhere
            return $this->$methodName();
        }

        $visibility = $this->getCallVisibility();
//var_dump("CALL_VISIBILITY", $visibility);
        if ($visibility === 2) {
            // Called from a private scope, so can access everything
            return $this->$methodName();
        } else if($visibility === 1) {
            // Called from a protected scope
            if ($ref->isPrivate()) {
                // Cant' access private properties
                throw $this->bumpError(new Error("Cannot access private getter ".static::class."::\$$name"));
            } else {
                return $this->$methodName();
            }
        } else {
            // Called from a public scope, can't access any protected or private properties
            throw $this->bumpError(new Error("Cannot access non-public getter ".static::class."::\$$name"));
        }
        return;
    }

    public function __set(string $name, $value) {
        $methodName = 'set_'.$name;

        // Do we have a setter?
        if (!method_exists($this, $methodName)) {
            if (property_exists($this, $name)) {
                // Error message is different if the property exists
                throw $this->bumpError(new Error("Cannot access private property ".static::class."::\$$name"));
            }
            trigger_error("Undefined property: ".static::class."::\$$name");
            return;
        }

        $ref = new ReflectionMethod($this, $methodName);

        if ($ref->isPublic()) {
            // Property is available everywhere
            return $this->$methodName($value);
        }

        $visibility = $this->getCallVisibility();
        if ($visibility === 2) {
            // Called from a private scope, so can access everything
            return $this->$methodName($value);
        } else if($visibility === 1) {
            // Called from a protected scope
            if ($ref->isPrivate()) {
                // Cant' access private properties
                throw $this->bumpError(new Error("Cannot access private getter ".static::class."::\$$name"));
            } else {
                return $this->$methodName($value);
            }
        } else {
            // Called from a public scope, can't access any protected or private properties
            throw $this->bumpError(new Error("Cannot access non-public getter ".static::class."::\$$name"));
        }
        return;
    }

    /**
     * Makes the error appear to come from the previous call stack element
     */
    private function bumpError(Error $error):Error {
        $ref = new ReflectionClass($error);

        $trace = $ref->getProperty('trace');
        $trace->setAccessible(true);
        $realTrace = $trace->getValue($error);

        if (isset($realTrace[0]['file'])) {
            $file = $ref->getProperty('file');
            $file->setAccessible(true);
            $file->setValue($error, $realTrace[0]['file']);
            $file->setAccessible(false);
        }

        if (isset($realTrace[0]['line'])) {
            $line = $ref->getProperty('line');
            $line->setAccessible(true);
            $line->setValue($error, $realTrace[0]['line']);
            $line->setAccessible(false);
        }

        array_shift($realTrace);
        $trace->setValue($error, $realTrace);

        $trace->setAccessible(false);

        return $error;
    }

    private function getCallVisibility():int {
        $trace = debug_backtrace(true, 3);
//var_dump($trace);
        if (!isset($trace[2]) || !isset($trace[2]['class'])) {
            return 0; // public visibility
        }
        $thatClass = $trace[2]['class'] ?? null;
        $thatObject = $trace[2]['object'] ?? null;

        if (static::class === $thatClass) {
            return 2; // private visibility
        }

        if (is_subclass_of($thatClass, static::class)) {
            return 1; // protected visibility
        }

        return true;
    }

}
