<?php
require(__DIR__."/../tests-include.php");
use function F2\{ expect, asserty };

class ChildWithGetSet extends TestParent {
    use F2\GetSet;

    private $testValue = "not touched";

    public function get_testValue(): string {
        return $this->testValue;
    }

    public function set_testValue(string $value): void {
        $this->testValue = $value;
    }
}


$i = new ChildWithGetSet();
asserty($i->publicProperty == "ok");
asserty($i->testValue == "not touched");
$i->testValue = "touched";
asserty($i->testValue === "touched");
