<?php
require(__DIR__."/../tests-include.php");
use function F2\expect;

expect(Error::class, function() {
    $i = new TestParent();
    $i->realProtectedProperty = "OK";
    $i->assertNotTouched("realProtectedProperty");
});
