<?php
require(__DIR__."/../tests-include.php");
use function F2\{ expect, asserty };

class ChildWithGetSet extends TestParent {
    use F2\GetSet {
        __get as getterGet;
    }

    public function __get($what) {
        if ($what != 'publicProperty') {
            return "from child getter";
        }
        return static::getterGet($what);
    }
}


$i = new ChildWithGetSet();
asserty($i->publicProperty == "ok");
asserty($i->otherProperty == 'from child getter');
