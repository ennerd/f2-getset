<?php
require(__DIR__."/vendor/autoload.php");
use function F2\asserty;
class TestParent {
    use F2\GetSet;

    public $data = [
        "public" => "not touched",
        "protected" => "not touched",
        "private" => "not touched",
    ];
    private $realPrivateProperty = "not touched";
    protected $realProtectedProperty = "not touched";

    public function get_publicProperty(): string {return "ok";}
    public function set_publicProperty(string $value): void {$this->data["public"] = $value;}
    protected function get_protectedProperty(): string {return "ok";}
    protected function set_protectedProperty(string $value): void {$this->data["protected"] = $value;}
    private function get_privateProperty(): string {return "ok";}
    private function set_privateProperty(string $value): void {$this->data["private"] = $value;}
    public function setPrivateFromParent(string $value): void {$this->privateProperty = $value;}
    public function setProtectedFromParent(string $value): void {$this->protectedProperty = $value;}
    public function setRealPrivateFromParent(string $value): void {$this->realPrivateProperty = $value;}
    public function setRealProtectedFromParent(string $value): void {$this->realProtectedProperty = $value;}

    public function assertTouched(string $name): void {
        $data = $this->getAll();
        asserty($data[$name]!=="not touched");
    }
    public function assertNotTouched(string $name): void {
        $data = $this->getAll();
        asserty($data[$name]==="not touched");
    }
    public function getAll(): array {
        return [
            "publicProperty" => $this->data["public"],
            "protectedProperty" => $this->data["protected"],
            "privateProperty" => $this->data["private"],
            "realProtectedProperty" => $this->realProtectedProperty,
            "realPrivateProperty" => $this->realPrivateProperty,
        ];
    }
}
class TestChild extends TestParent {
    public function setRealPrivateFromChild(string $value): void {
        $this->realPrivateProperty = $value;
    }
    public function setRealProtectedFromChild(string $value): void {
        $this->realProtectedProperty = $value;
    }
}


