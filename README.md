# f2/getset

Simple library that enables creating getters and setters with PHP in a consistent manner. Supports visibility and type hinting.

## Simple getter and setter

```php
<?php
class MyClass {
    use F2\GetSet;

    private $secret_value = "Initial value";

    /**
     * Any method that starts with "get_" is automatically a getter
     *
     * public, protected and private visibility is accepted and enforced.
     * Type checking is enforced by PHP automatically.
     */
    public function get_value(): string {
        return $this->secret_value;
    }

    /**
     * Any method that starts with "set_" is automatically a getter
     *
     * public, protected and private visibility is accepted and enforced.
     * Type checking is enforced by PHP automatically.
     */
    public function set_value(string $value): void {
        $this->secret_value = $value;
    }
}

$instance = new MyClass();

$instance->value .= ' and some more';
// Log: The property 'value' was read
// Log: The property 'value' was set to 'Initial value and some more'

## No dependencies

This library has no dependencies (except a development dependency f2/asserty)
